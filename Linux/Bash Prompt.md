# Customising the Command Prompt

## Prompt Basics

Use the echo command to display the current prompt setting:

    $ echo $PS1 

## Colours

To add colours to the shell prompt use the following export command syntax:

    '\e[x;ym $PS1 \e[m'

Where,

    \e[ : Start color scheme.
    x;y : Color pair to use (x;y)
    $PS1 : Your shell prompt variable.
    \e[m : Stop color scheme.

To set a red colour prompt, type the following command:

    $ export PS1="\e[0;31m[\u@\h \W]\$ \e[m "

Here is a list of all of the colours 

    0;30m   Black
    0;31m   Red
    0;32m   Green
    0;33m   Yellow
    0;34m   Blue
    0;35m   Purple
    0;36m   Cyan
    0;37m   White

0 is normal font. Change the 0 to 1 for bold, 4 for underline, and 5 for slow blink.

## Example Prompt

    export PS1="\[\e[0;35m\]\u\[\e[0;0m\]{\[\e[4;33m\]\!\[\e[0;0m\]}\[\e[0;0m\]-\[\e[0;32m\]\t\[\e[0;0m\] (\[\e[01;31m\]\W\[\e[0;0m\])\[\033[01;31m\]\$\[\e[0;0m\] "

# Web frameworks

| Link                 | Notes                                                               |
|----------------------|---------------------------------------------------------------------|
| https://biffweb.com/ | Biff is a batteries-included web framework for Clojure. #htmx #xtdb |
